#include <string>
#include <iostream>
#include <list>
#include <cstdlib>

using namespace std;

class Matrix
{
private:
    int m;
    int n;
    int **array;
    int *parseLine(string line, int characterIndex);
    string format(int number);

public:
    Matrix(string matrixString);
    Matrix(int m, int n, int **array);
    Matrix operator+(const Matrix &matrix) const;
    Matrix operator-(const Matrix &matrix) const;
    Matrix operator*(const Matrix &matrix) const;
    Matrix transpose();
    void printMatrix();
};

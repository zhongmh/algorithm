#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <stack>
#include "matrix.hpp"

using namespace std;

string readMatrixFile(char *file)
{
    ifstream fin;
    string str;
    fin.open(file);
    char c;
    if (fin.is_open())
    {
        while (fin.get(c))
        {
            str.append(&c, 1);
        }
    }
    ofstream fout(file, ios::out | ios::app);
    if (!fout.is_open())
    {
        cerr << "Can't open " << file << " file for output.\n";
        exit(EXIT_FAILURE);
    }
    return str;
}

int main()
{
    stack<Matrix> matricStack;
    int op;
    char ch;
    while (1) {
        cout << "请选择矩阵操作 1 加法 2 减法 3 乘法 4 转置 f 矩阵文件 = 输出结果 c 上一次输入 p 打印栈顶矩阵 q 退出" << endl;
        cin >> ch;
        if ('1' == ch || '2' == ch || '3' == ch || '4' == ch) {
            op = atoi(&ch);
        }else if('f' == ch){
            while (1) {
                string file;
                cout << "请输入文件路口，q退出：";
                cin >> file;
                if ("q" == file)break;
                ofstream fout(file, ios::out | ios::app);
                if (!fout.is_open())
                {
                    cout << "文件" << file << "不存在，请重新输入" << endl;;
                    continue;
                }
                string matrixString = readMatrixFile(file.data());
                Matrix matrix(matrixString);
                matricStack.push(matrix);
            }
        }
        else if ('c' == ch) {
            if (matricStack.empty()) {
                continue;
            }
            matricStack.pop();
        }
        else if('=' == ch) {
            if ((op != 4 && matricStack.size() < 2) || (op == 4 && matricStack.empty())) {
                cout << "当前矩阵数量为:" << matricStack.size() << ",请输入矩阵" << endl;
               continue; 
            }
            if (op == 1) {
                Matrix matrix2 = matricStack.top();
                matricStack.pop();
                Matrix matrix1 = matricStack.top();
                matricStack.pop();
                Matrix matrix = matrix1 + matrix2;
                matricStack.push(matrix);
                matrix.printMatrix();
            }
            if (op == 2) {
                Matrix matrix2 = matricStack.top();
                matricStack.pop();
                Matrix matrix1 = matricStack.top();
                matricStack.pop();
                Matrix matrix = matrix1 - matrix2;
                matricStack.push(matrix);
                matrix.printMatrix();
            }
            if (op == 3) {
                Matrix matrix2 = matricStack.top();
                matricStack.pop();
                Matrix matrix1 = matricStack.top();
                matricStack.pop();
                Matrix matrix = matrix1 * matrix2;
                matricStack.push(matrix);
                matrix.printMatrix();
            }
            if (op == 4) {
                Matrix matrix = matricStack.top();
                matricStack.pop();
                matrix = matrix.transpose();
                matricStack.push(matrix);
                matrix.printMatrix();
            }
        }
        else if ('p' == ch) {
            if (matricStack.empty()) {
                cout << "当前没有矩阵\n";
                continue;
            }
            Matrix matrix = matricStack.top();
            matrix.printMatrix();
        }
        else if ('q' == ch) {
            break;
        }
    }
    return 0;
}
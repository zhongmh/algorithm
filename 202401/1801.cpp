#include <iostream>

using namespace std;

#define T 0
#define B 5
#define S 1
#define E 2
#define W 3
#define N 4

/**
 * T <- W <- B <- E <- T
 */
void rollToE(int *dice, int *mirrorDice)
{
    /**
     * S 和 N 不变
     */
    mirrorDice[S] = dice[S];
    mirrorDice[N] = dice[N];
    mirrorDice[T] = dice[W];
    mirrorDice[W] = dice[B];
    mirrorDice[B] = dice[E];
    mirrorDice[E] = dice[T];
}

/**
 * T <- N <- B <- S <-T
 */
void rollToS(int *dice, int *mirrorDice)
{
    /**
     * W 和 E 保持不变
     */
    mirrorDice[W] = dice[W];
    mirrorDice[E] = dice[E];
    mirrorDice[T] = dice[N];
    mirrorDice[N] = dice[B];
    mirrorDice[B] = dice[S];
    mirrorDice[S] = dice[T];
}

void rotateToClockwise(int *dice, int *mirrorDice)
{
    /**
     * T 和 B 保持不变
     */
    mirrorDice[T] = dice[T];
    mirrorDice[B] = dice[B];
    mirrorDice[S] = dice[E];
    mirrorDice[E] = dice[N];
    mirrorDice[N] = dice[W];
    mirrorDice[W] = dice[S];
}

void copy(int *dice, int *mirrorDice)
{
    for (int i = 0; i < 6; i++)
        dice[i] = mirrorDice[i];
}

int equal(int *dice1, int *dice2)
{
    return (dice1[0] == dice2[0] && dice1[1] == dice2[1] && dice1[2] == dice2[2] && dice1[3] == dice2[3] && dice1[4] == dice2[4] && dice1[5] == dice2[5]);
}

int isSameDice(int *dice0, int *dice1)
{
    int dice2[6], dice3[6], dice4[6];
    for (int i = 0; i < 4; i++)
    {

        if (equal(dice0, dice1))
        {
            return 1;
        }
        // 向E方向翻滚
        rollToE(dice1, dice2);
        // printDice("向E方向旋转", dice2);
        for (int j = 1; j < 4; j++)
        {

            if (equal(dice0, dice2))
            {
                return 1;
            }
            // 顺时针方向旋转
            rotateToClockwise(dice2, dice3);
            for (int k = 0; k < 4; k++)
            {
                if (equal(dice0, dice3))
                {
                    return 1;
                }

                if (k == 3)
                    break;
                rotateToClockwise(dice3, dice4);
                // 写回第三层
                copy(dice3, dice4);
            }

            if (j == 3)
                break;
            rollToE(dice2, dice3);
            // 写回第二层
            copy(dice2, dice3);
        }
        if (i == 3)
            break;
        rollToS(dice1, dice2);
        copy(dice1, dice2);
    }
    return 0;
}

int main()
{
    int n;
    cin >> n;
    int dices[n][6];
    for (int i = 0; i < n; i++)
        for (int k = 0; k < 6; k++)
            cin >> dices[i][k];
    for (int i = 0; i < n; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (isSameDice(dices[i], dices[j]))
            {
                cout << "Yes" << endl;
                return 0;
            }
        }
    }
    cout << "No" << endl;
    return 0;
}
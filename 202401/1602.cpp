#include <iostream>

using namespace std;

#define T 0
#define B 5
#define S 1
#define E 2
#define W 3
#define N 4

/**
 * T <- W <- B <- E <- T
 */
void rollToE(int *dice, int *mirrorDice)
{
    /**
     * S 和 N 不变
     */
    mirrorDice[S] = dice[S];
    mirrorDice[N] = dice[N];
    mirrorDice[T] = dice[W];
    mirrorDice[W] = dice[B];
    mirrorDice[B] = dice[E];
    mirrorDice[E] = dice[T];
}

/**
 * T <- N <- B <- S <-T
 */
void rollToS(int *dice, int *mirrorDice)
{
    /**
     * W 和 E 保持不变
     */
    mirrorDice[W] = dice[W];
    mirrorDice[E] = dice[E];
    mirrorDice[T] = dice[N];
    mirrorDice[N] = dice[B];
    mirrorDice[B] = dice[S];
    mirrorDice[S] = dice[T];
}

void rotateToClockwise(int *dice, int *mirrorDice)
{
    /**
     * T 和 B 保持不变
     */
    mirrorDice[T] = dice[T];
    mirrorDice[B] = dice[B];
    mirrorDice[S] = dice[E];
    mirrorDice[E] = dice[N];
    mirrorDice[N] = dice[W];
    mirrorDice[W] = dice[S];
}

void copy(int *dice, int *mirrorDice)
{
    for (int i = 0; i < 6; i++)
        dice[i] = mirrorDice[i];
}

int equal(int *dice1, int *dice2)
{
    return (dice1[0] == dice2[0] && dice1[1] == dice2[1] && dice1[2] == dice2[2] && dice1[3] == dice2[3] && dice1[4] == dice2[4] && dice1[5] == dice2[5]);
}

int main()
{
    /**
     * 定义一个骰子的6个面
     * 0 T
     * 1 S
     * 2 E
     * 3 W
     * 4 N
     * 5 B
     */
    // 2 E->W
    // 3,4 N->S
    // 5,6 顺时针旋转
    int dices[5][6];
    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 6; j++)
            cin >> dices[i][j];
    for (int i = 0; i < 4; i++)
    {

        if (equal(dices[0], dices[1]))
        {
            cout << "Yes" << endl;
            return 0;
        }
        // 向E方向翻滚
        rollToE(dices[1], dices[2]);
        for (int j = 1; j < 4; j++)
        {

            if (equal(dices[0], dices[2]))
            {
                cout << "Yes" << endl;
                return 0;
            }
            // 、顺时针方向旋转
            rotateToClockwise(dices[2], dices[3]);
            for (int k = 0; k < 3; k++)
            {
                if (equal(dices[0], dices[3]))
                {
                    cout << "Yes" << endl;
                    return 0;
                }

                if (k == 2)
                    break;
                rotateToClockwise(dices[3], dices[4]);
                // 写回第三层
                copy(dices[3], dices[4]);
            }

            if (j == 3)
                break;
            rollToE(dices[2], dices[3]);
            // 写回第二层
            copy(dices[2], dices[3]);
        }
        if (i == 3)
            break;
        rollToS(dices[1], dices[2]);
        copy(dices[1], dices[2]);
    }
    cout << "No" << endl;
    return 0;
}
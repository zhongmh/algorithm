#include <iostream>
#include <stdlib.h>
#include <queue>

using namespace std;

struct process
{
	char name[10];	  // 线程名称
	int worktime;	  // 工作时间
	int waittime = 0; // 等待时间
};

int main()
{
	int n, q;
	cin >> n >> q;
	queue<process *> waitingQueue;
	queue<process *> completedQueue;

	char name[10];
	int times;
	while (n--)
	{
		process *p = new process;
		cin >> p->name >> p->worktime;
		waitingQueue.push(p);
	}
	int totalWorkTime = 0, worktime;
	for (int timeSegment = q; !waitingQueue.empty(); timeSegment = q)
	{
		process *p = waitingQueue.front();
		waitingQueue.pop();
		n = waitingQueue.size();
		worktime = p->worktime > timeSegment ? timeSegment : p->worktime;
		totalWorkTime += worktime;
		p->worktime = p->worktime - worktime;
		if (p->worktime == 0)
		{
			completedQueue.push(p);
			p->waittime = totalWorkTime;
		}
		else
			waitingQueue.push(p);
	}
	while (!completedQueue.empty())
	{
		process *p = completedQueue.front();
		completedQueue.pop();
		cout << p->name << " " << p->waittime << endl;
	}
}

#include <iostream>
#include <stack>
#include <string>
using namespace std;

int main()
{
	stack<int> stack;
	string expresion, found_str;
	getline(cin, expresion);
	int len = expresion.length(), a, b;
	string buffer;
	for (int i = 0; i < len; i++)
	{
		const char c = expresion.at(i);
		if (' ' == c || i + 1 == len)
		{
			if(i+1 == len){
				buffer.append(&c,1);
			}
			if ("*" == buffer)
			{
				b = stack.top();
				stack.pop();
				a = stack.top();
				stack.pop();
				stack.push(a * b);
			}
			else if ("+" == buffer)
			{
				b = stack.top();
				stack.pop();
				a = stack.top();
				stack.pop();
				stack.push(a + b);
			}
			else if ("-" == buffer)
			{
				b = stack.top();
				stack.pop();
				a = stack.top();
				stack.pop();
				stack.push(a - b);
			}
			else
			{
				stack.push(stoi(buffer));
			}
			buffer.clear();
		}
		else
		{
			buffer.append(&c, 1);
		}
	}
	cout << stack.top() << endl;
}

#include <iostream>
#include <string>
#include <cmath>
#include <vector>

using namespace std;


/**
 * 生成汉明码
 * n 有效位
 * k 校验位
 *
 */
void generateHammingCode(int bits[], int len)
{
	int k = 3;
	if (len <= 8)
		k = 5;
	else if (len <= 16)
		k = 6;
	else if (len <= 32)
		k = 5;
	else if (len <= 64)
		k = 6;
	else if (len <= 128)
		k = 7;
	else if (len <= 256)
		k = 8;
	cout << "校验位k=" << k << endl;
	// 声明汉明编码数组
	int hamming_code_bits[len + k];
	// 汉明码校验位;用来确定校验位的参数
	int checks[k][k+len];
	
}

/**
 *校验汉明码
 *
 */
void checkHammingCode(int *bits[], int len)
{
}
int main(int argc, char *argv[])
{
	if (argc == 3)
	{
		string opt(argv[1]);
		string binaryCode(argv[2]);
		if ("-g" == opt)
		{
			int bits[binaryCode.length()];
			for (int i = 0; i < binaryCode.length(); i++)
			{
				const char a = binaryCode.at(i);
				if ('0' == a)
				{
					bits[i] = 0;
				}
				else if ('1' == a)
				{
					bits[i] = 1;
				}
				else
				{
					cout << "非法的二进制参数:" + binaryCode << endl;
					return 0;
				}
			}
			generateHammingCode(bits, binaryCode.length());
		}
		else if ("-c" == opt)
		{
			cout << "校验汉明码" << endl;
		}
		else
		{
			cout << "非法参数" << endl;
		}
	}
	else
	{
		cout << "参数错误" << endl;
	}
}

#include <string>
#include <iostream>
#include <ctime>
#include <stdio.h>

using namespace std;

struct node
{
	node *pre;
	node *next;
	int data;
};

class DoublyLink
{
protected:
	node *head;
	node *tail;

public:
	DoublyLink();
	void insert(int data);
	void delNode(int data);
	void deleteFirst();
	void deleteLast();
	void printLink();
};

int main()
{
	DoublyLink dl;
	int n, number;
	string opt;
	cin >> n;
	while (n--)
	{
		cin >> opt;
		if ("insert" == opt)
		{
			cin >> number;
			dl.insert(number);
		}
		else if ("delete" == opt)
		{
			cin >> number;\
			dl.delNode(number);
		}
		else if ("deleteFirst" == opt)
		{
			dl.deleteFirst();
		}
		else if ("deleteLast" == opt)
		{
			dl.deleteLast();
		}
		else
		{
			dl.printLink();
		}
	}
	dl.printLink();
}

DoublyLink::DoubleLink()
{
	head = new node;
	tail = new node;
	head->next = tail;
	tail->pre = head;
}

void DoublyLink::insert(int data)
{
	node *newnode = new node;
	newnode->data = data;
	head->next->pre = newnode;
	newnode->next = head->next;
	newnode->pre = head;
	head->next = newnode;
}
void DoublyLink::delNode(int data)
{
	node *node = head->next;
	while (node != tail)
	{
		if (node->data == data)
		{
			node->next->pre = node->pre;
			node->pre->next = node->next;
			delete node;
			break;
		}
		node = node->next;
	}
}

void DoublyLink::deleteFirst()
{
	node *node = head->next;
	if (node != tail)
	{
		head->next = node->next;
		node->next->pre = head;
		delete node;
	}
}

void DoublyLink::deleteLast()
{
	node *node = tail->pre;
	if (node != tail)
	{
		node->pre->next = tail;
		tail->pre = node->pre;
		delete node;
	}
}

void DoublyLink::printLink()
{
	node *node = head->next;
	if (node == tail)
	{
		return;
	}
	while (1)
	{
		cout << node->data;
		node = node->next;
		if (node == tail)
		{
			cout << endl;
			break;
		}
		cout << " ";
	}
}

#include <iostream>
#include <string>
#include <stack>

using namespace std;

int main(){
	string input;
	cin >> input;
	stack<int> stack;
	int len = input.length();
	for(int i=0;i<len;i++){
		const char c = input.at(i);
		if(stack.empty() && ('-' == c || '/' == c)){
			continue;
		}
		if('\\' == c){
		 	stack.push(-1);
		}else if('/' == c){
			stack.push(1);
		}else if('—' == c){
			stack.push(0);
		}
	}
}

#include <iostream>
#include <cmath>
/**
 * 计算不同时期的投资收益
*/
using namespace std;

/**
 * 递归实现
 */
int a(int *arr, int i, int s)
{
    if (s == 0 && i == 1)
    {
        return arr[i] - arr[s];
    }
    if (s == 0)
    {
        return max(arr[i] - arr[s], a(arr, i - 1, i - 2));
    }
    return max(arr[i] - arr[s], a(arr, i, s - 1));
}

int main()
{
    int n, m, minT, maxP;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> m;
        if (i > 1)
        {
            if (m >= minT)
            {
                maxP = max(maxP, m - minT);
            }
            else
            {
                minT = m;
            }
        }
        else if (i == 1)
        {
            maxP = m - minT;
            minT = min(minT, m);
        }
        else
        {
            minT = m;
        }
    }

    cout << maxP << endl;
}

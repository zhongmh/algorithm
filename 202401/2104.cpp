#include <iostream>

using namespace std;
int cnt = 0;
void insertionSort(int array[], int n, int g);
void shellSort(int array[], int n);
int main()
{
    int n;
    cin >> n;
    int array[n];
    for (int i = 0; i < n; i++)
        cin >> array[i];
    shellSort(array, n);
    cout << cnt << endl;
    for (int i = 0; i < n; i++)
    {
        cout << array[i] << endl;
    }
}

void shellSort(int array[], int n)
{
    int m = 0,s = 1;
	while((n/s) > 0){
		s *= 2;
		m++;
	}
	int G[m];
	for(int i= 0,t = 1;i<m;i++){
		G[i] = n / t;
		t *= 2;
	}
    for (int i = 0;i < m;i++)
        insertionSort(array, n, G[i]);
    cout << m << endl;
    for (int i = 0; i < m; i++)
    {
        cout << G[i];
        if (i + 1 == m)
            cout << endl;
        else
            cout << " ";
    }
}

void insertionSort(int array[], int n, int g)
{
    for (int i = g, j = 0; i < n; i++)
    {
        int v = array[i];
		j = i - g;
        while (j >= 0 && array[j] > v)
        {
            array[j + g] = array[j];
            j = j - g;
            cnt++;
        }
        array[j + g] = v;
    }
}

#include <iostream>
#include <cstring>
using namespace std;
/**
 * 定义一个骰子的6个面
 * 0 T
 * 1 S
 * 2 E
 * 3 W
 * 4 N
 * 5 B
 */
int diceFaces[6];
/**
 * T <- W <- B <- E <- T
 */
void rollToE()
{
    /**
     * S 和 N 不变
     */
    int tmp = diceFaces[0];
    diceFaces[0] = diceFaces[3];
    diceFaces[3] = diceFaces[5];
    diceFaces[5] = diceFaces[2];
    diceFaces[2] = tmp;
}

void rollToW()
{
    int tmp = diceFaces[0];
    diceFaces[0] = diceFaces[2];
    diceFaces[2] = diceFaces[5];
    diceFaces[5] = diceFaces[3];
    diceFaces[3] = tmp;
}

/**
 * T <- N <- B <- S <-T
 */
void rollToS()
{
    /**
     * W 和 E 保持不变
     */
    int tmp = diceFaces[0];
    diceFaces[0] = diceFaces[4];
    diceFaces[4] = diceFaces[5];
    diceFaces[5] = diceFaces[1];
    diceFaces[1] = tmp;
}

/**
 * 向北滚动
 */
void rollToN()
{
    int tmp = diceFaces[0];
    diceFaces[0] = diceFaces[1];
    diceFaces[1] = diceFaces[5];
    diceFaces[5] = diceFaces[4];
    diceFaces[4] = tmp;
}

int main()
{
    for (int i = 0; i < 6; i++)
        cin >> diceFaces[i];
    string command;
    cin >> command;
    int len = command.length();
    for (int i = 0; i < len; i++)
    {
        if ('E' == command[i])
            rollToE();
        else if ('W' == command[i])
            rollToW();
        else if ('S' == command[i])
            rollToS();
        else if ('N' == command[i])
            rollToN();
    }
    cout << diceFaces[0] << endl;
}

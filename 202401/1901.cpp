#include <iostream>
#include <cmath>
using namespace std;

/**
 * 计算两个数的最大公因数
*/
int main(){
    int a,b;
    cin >> a >> b;
    for(int tmp=a-b;;tmp = a-b){
        if(tmp == 0){
            break;
        }
        if(tmp < 0){
            a = b;
            b = abs(tmp);
        }else{
             a = tmp;
        }
    }
    cout << b << endl;
}
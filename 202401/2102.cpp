#include <iostream>

using namespace std;

/**
 * Selection Sort
 */
int main()
{
    int swap_times = 0, n, mini, tmp;
    cin >> n;
    int array[n];
    for (int i = 0; i < n; i++)
        cin >> array[i];
    for (int i = 0; i < n; i++)
    {
        mini = i;
        for (int j = i + 1; j < n; j++)
        {
            if (array[j] < array[mini])
            {
                mini = j;
            }
        }
        if (i != mini)
        {
            tmp = array[i];
            array[i] = array[mini];
            array[mini] = tmp;
            swap_times++;
        }
    }
    for (int i = 0; i < n; i++)
    {
        cout << array[i];
        if (i == n - 1)
        {
            cout << endl;
        }
        else
        {
            cout << " ";
        }
    }
    cout << swap_times << endl;
}
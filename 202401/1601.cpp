#include <iostream>
#include <stdio.h>
using namespace std;

/**
 * 定义一个骰子的6个面
 * 0 T
 * 1 S
 * 2 E
 * 3 W
 * 4 N
 * 5 B
 */
int diceFaces[6];
/**
 * T <- W <- B <- E <- T
 */
void rollToE()
{
    /**
     * S 和 N 不变
     */
    int tmp = diceFaces[0];
    diceFaces[0] = diceFaces[3];
    diceFaces[3] = diceFaces[5];
    diceFaces[5] = diceFaces[2];
    diceFaces[2] = tmp;
}

void rollToW()
{
    int tmp = diceFaces[0];
    diceFaces[0] = diceFaces[2];
    diceFaces[2] = diceFaces[5];
    diceFaces[5] = diceFaces[3];
    diceFaces[3] = tmp;
}

/**
 * T <- N <- B <- S <-T
 */
void rollToS()
{
    /**
     * W 和 E 保持不变
     */
    int tmp = diceFaces[0];
    diceFaces[0] = diceFaces[4];
    diceFaces[4] = diceFaces[5];
    diceFaces[5] = diceFaces[1];
    diceFaces[1] = tmp;
}

/**
 * 向北滚动
 */
void rollToN()
{
    int tmp = diceFaces[0];
    diceFaces[0] = diceFaces[1];
    diceFaces[1] = diceFaces[5];
    diceFaces[5] = diceFaces[4];
    diceFaces[4] = tmp;
}

void rotateToClockwise(){
    int tmp = diceFaces[1];
    diceFaces[1] = diceFaces[2];
    diceFaces[2] = diceFaces[4];
    diceFaces[4] = diceFaces[3];
    diceFaces[3] = tmp;
}

void rotateToAnticlockwise(){
    int tmp = diceFaces[1];
    diceFaces[1] = diceFaces[3];
    diceFaces[3] = diceFaces[4];
    diceFaces[4] = diceFaces[2];
    diceFaces[2] = tmp;
}

int main(){
    int n,ts,fs;
    for (int i = 0; i < 6; i++)
        cin >> diceFaces[i];
    cin >> n;
    while(n--){
        cin >> ts >> fs;
        if(diceFaces[0] != ts){
            if(diceFaces[5] == ts){ // B
                rollToN();
                rollToN();
            }
            if(diceFaces[1] == ts){ // S
                rollToN();
            }
            if(diceFaces[2] == ts){ // E
                rollToW();
            }
            if(diceFaces[3] == ts){ // W
                rollToE();
            }
            if(diceFaces[4] == ts){ // N
                rollToS();
            }
        }
        if(diceFaces[1] != fs){
            if(diceFaces[3] == fs){
                rotateToAnticlockwise();
            }
            if(diceFaces[2] == fs){
                rotateToClockwise();
            }
            if(diceFaces[4] == fs){
                rotateToClockwise();
                rotateToClockwise();
            } 
        }
        printf("%d\n",diceFaces[2]);
    }
}
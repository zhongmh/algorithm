#include <iostream>
#include <cstring>
#define R 6
/**
 * 数据串：1101011011
 * 多项式：x^6 + x^4 + x^2 + x + 1
 */
int p[7] = {1,0,1,0,1,1,1};
using namespace std;

void printBits(int bits[],int len){
	for(int i=0;i<len;i++){
		cout << bits[i];
	}
	cout << endl;
}

void generate(int bits[],int len){
	int crcbits[len + R];
	for(int i=0;i<len;i++){
		crcbits[i] = bits[i];
	}
	//这里相当于将二进制数左移R位
	for(int i=len + R;i >= len; i--){
		crcbits[i] = 0;
	}
	cout << "CRC校验码数组：";
	printBits(crcbits,len+R);
	cout << "多项式：";
	printBits(p,7);
	cout << "信息码：";
	printBits(bits,len);

	for(int i=0;i<len;i++){
		if(bits[i] == 1 || (i + 7 + 1 == len)){
			for(int j = 0;j < 7;j++){
				if(bits[i+j] == p[j])
					 bits[i+j] = 0;
				else
					 bits[i+j] = 1;
			}
			printBits(bits,len);
		}
	}
	cout << "校验码：";
	printBits(bits,len);
}

int main(int argc,char* argv[]){
	if(argc == 3){
		string opt(argv[1]);
		string binaryCode(argv[2]);
		int bits[binaryCode.length()];
		for(int i=0;i<binaryCode.length();i++){
			const char a = binaryCode.at(i);
			if('0' == a){
				bits[i] = 0;
			}else if('1' == a){
				bits[i] = 1;
			}else {
				cout << "错误的二进制码：" << endl;
			}
		}
		generate(bits,binaryCode.length());
	}
}

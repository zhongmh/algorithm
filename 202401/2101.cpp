#include <iostream>

using namespace std;

/**
 * Bubble Sort
*/
int main()
{
    int number=0, n, m;
    cin >> n;
    int array[n];
    for (int i = 0; i < n; i++)
        cin >> array[i];
    for (int i = 0; i < n; i++)
        for (int j = n - 1; j > i; j--)
        {
            if (array[j] < array[j - 1])
            {
                m = array[j];
                array[j] = array[j - 1];
                array[j - 1] = m;
                number++;
            }
        }
    for (int i = 0; i < n; i++)
    {
        cout << array[i];
        if (i == n - 1)
        {
            cout << endl;
        }
        else
        {
            cout << " ";
        }
    }
    cout << number << endl;
}
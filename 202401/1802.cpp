#include <iostream>

using namespace std;

int main()
{
    int n,j,tmp;
    cin >> n;
    int array[n];
    for (int i = 0; i < n; i++)
        cin >> array[i];
    for(int i=0;i<n;i++){
        tmp = array[i];
        j=i-1;
        while(j  >=0 && array[j] > tmp){
            array[j+1] = array[j];
            j--;
        }
        array[j+1] = tmp;
        // print
        for(int i=0;i<n;i++){
            cout << array[i];
            if(i == n-1)
                cout << endl;
            else 
                cout << " ";
        }
    }
}
#define MAXLEN 3
typedef struct {
    char id[10];
    char name[20];
    int age;
} User;

typedef struct Stack {
    User* users[MAXLEN];
    int end;
} Stack;

Stack* StackInit();

int StackAdd(Stack* stack,User* user);

int IsFull(Stack* stack);

int IsEmpty(Stack* stack);

int Push(Stack* stack,User* user);

User* Pop(Stack* stack);
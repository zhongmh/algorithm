#include "Stack.h"
#include <stdlib.h>
#include <iostream>
using namespace std;

Stack* StackInit() {
    Stack* stack;
    stack = (Stack*)malloc(sizeof(Stack));
    stack->end = 0;
    return stack;
}

int IsFull(Stack* stack) {
    return stack->end == MAXLEN;
}

int IsEmpty(Stack* stack) {
    return 0 == stack->end;
}

int Push(Stack* stack, User* user) {
    if (IsFull(stack)) {
        cout << "没有足够的空间了" << endl;
        return 0;
    }
    stack->users[stack->end++];
    return 1;
}

User* Pop(Stack* stack) {
    if (IsEmpty(stack)) {
        return NULL;
    }
    stack->end--;
    return stack->users[stack->end];
}
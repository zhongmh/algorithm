#include "Stack.h"
#include <iostream>
#include <stdio.h>

using namespace std;

int main() {
    Stack* stack = StackInit();;
    char key[10];
    cout << "栈操作演示！" << endl;
    cout << "栈初始化完成" << endl;
    int o;
    cout << "输入你的操作：" << endl;
    cout << "0 Push节点" << endl;
    cout << "1 Pop节点" << endl;
    cout << "-1 退出" << endl;
    do {
        cout << "输入你的操作:";
        fflush(stdin);
        scanf("%d", &o);
        if (o == -1) {
            break;
        }
        if (o == 0) {
            fflush(stdin);
            cout << "输入你要Push的数据:";
            User* user;
            user = (User*)malloc(sizeof(User));
            scanf("%s%s%d", user->id, user->name, &user->age);
            Push(stack, user);
            cout << "添加成功" << endl;
        }
        if (o == 1) {
            fflush(stdin);
            cout << "你要Pop的数据:";
            User* user = Pop(stack);
            if (user == NULL)
                cout << "(Null)" << endl;
            else
                cout << "(" << user->id << "," << user->name << "," << user->age << ")" << endl;
        }

    } while (1);
    cout << "演示结束" << endl;
    return 0;
}
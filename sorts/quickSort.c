#include <stdio.h>
#define  SWAP(a,b){ int tmp; tmp = a;a = b;b = tmp;}

void quick_sort_sub(int *data,int left,int right){
	int left_index = left;
	int right_index = right;
	int pivot = data[(right + left)/2];
	while(left_index <= right_index){
		for(;data[left_index] < pivot;left_index++);
		for(;data[right_index] > pivot;right_index--);
		if(left_index <= right_index){
			SWAP(data[left_index],data[right_index]);
			left_index++;
			right_index--;
		}		
	}
	if(right_index > left){
		quick_sort_sub(data,left,right_index);
	}
	if(left_index < right){
		quick_sort_sub(data,left_index,right);
	}
}
void  quick_sort(int *data,int data_size)
{
	quick_sort_sub(data,0,data_size-1);
}

int main(){
	int data[] = {10,1,4,8,2,4,34,23,12,32};
	quick_sort(data,10);
	for(int i=0;i<10;i++){
		printf("%d\t",data[i]);
	}
	printf("\n");
	printf("data[2]=%d\n",*(data + 2));
	printf("data[4]=%d\n",*(data + 4));
}

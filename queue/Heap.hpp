class Heap
{
private:
    int count;
    int capacity;
    int heap_type;
    int array[100];
    void resizeHeap();

public:
    Heap(int capacity, int heap_type);
    int parent(int i);
    int leftChild(int i);
    int rightChild(int i);
    void percolateDown(int i);
    int insert(int data);
    void print();
};
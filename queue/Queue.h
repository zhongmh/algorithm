#define MAXLEN 3
typedef struct {
    char id[10];
    char name[20];
    int age;
} User;

typedef struct Queue {
    User* users[MAXLEN];
    int index;
    int end;
} Queue;

Queue* QueueInit();

int QueueAdd(Queue* queue,User* user);

int IsFull(Queue* queue);

int IsEmpty(Queue* queue);

int Push(Queue* queue,User* user);

User* Pop(Queue* queue);
#include "Queue.h"
#include <stdlib.h>
#include <iostream>
using namespace std;

Queue* queueInit() {
    Queue* queue;
    queue = (Queue*)malloc(sizeof(Queue));
    queue->end = 0;
    queue->index = 0;
    return queue;
}

int QueueAdd(Queue* queue, User* user) {
    if (queue->end == MAXLEN) {
        if (IsFull(queue)) {
            cout << "没有足够的空间了" << endl;
            return -1;
        }
        int i;
        for (i = 0;queue->index < MAXLEN;i++, queue->index++) {
            queue->users[i] = queue->users[queue->index];
        }
        queue->index = 0;
        queue->end = i;
    }
    queue->users[queue->end] = user;
    queue->end++;
    cout << "当前尾索引：" << queue->end << endl;
    return 0;
}

int IsFull(Queue* queue) {
    return queue->index == 0 && queue->end == MAXLEN;
}

int IsEmpty(Queue* queue) {
    return queue->index == queue->end;
}

int Push(Queue* queue, User* user) {
    if (IsFull(queue)) {
        cout << "没有足够的空间了" << endl;
        return 0;
    }
    return QueueAdd(queue, user);
}

User* Pop(Queue* queue) {
    if (IsEmpty(queue)) {
        return NULL;
    }
    return queue->users[queue->index++];
}
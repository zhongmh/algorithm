#include "Heap.hpp"
#include <stdio.h>

Heap::Heap(int capacity, int heap_type)
{
    this->heap_type = heap_type;
    this->capacity = capacity;
    this->count = 0;
}

int Heap::parent(int i)
{
    if (i <= 0 || i >= count)
        return -1;
    return i - 1 / 2;
}

int Heap::leftChild(int i)
{
    int left = 2 * i + 1;
    return left >= this->count ? -1 : left;
}

int Heap::rightChild(int i)
{
    int left = 2 * i + 2;
    return left >= this->count ? -1 : left;
}

void Heap::percolateDown(int i)
{
    int l, r, max, temp;
    l = leftChild(i);
    r = rightChild(i);
    if (l != -1 && this->array[l] > this->array[r])
        max = l;
    else
        max = r;
    if (r != -1 && this->array[r] > this->array[l])
        max = r;
    if (max != i)
    {
        temp = this->array[i];
        this->array[i] = this->array[max];
        this->array[max] = temp;
    }
    percolateDown(max);
}

void Heap::resizeHeap()
{
}

int Heap::insert(int data)
{
    int i;
    if (this->count == this->capacity)
        resizeHeap();
    this->count++;
    i = this->count - 1;
    while (i >= 0 && data > this->array[(i - 1) / 2])
    {
        this->array[i] = this->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    this->array[i] = data;
    return i;
}

void Heap::print()
{
    int i;
    printf("[");
    for (i = 0; i < this->count; i++)
        printf("%d,", this->array[i]);
    printf("]\n");
}
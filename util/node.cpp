#include "node.hpp"

template <typename T>
node<T>::node(T data)
{
    _data = data;
}

template <typename T>
node<T>::node(T data, node<T> *pre, node<T> *next)
{
    _data = data;
    _preNode = pre;
    _nextNode = next;
}

template <typename T>
void node<T>::setPreNode(node<T> *preNode)
{
    _preNode = preNode;
}

template <typename T>
void node<T>::setNextNode(node<T> *nextNode)
{
    _nextNode = nextNode;
}

template <typename T>
node<T> *node<T>::getPreNode()
{
    return this->_preNode;
}

template <typename T>
node<T> *node<T>::getNextNode()
{
    return this->_nextNode;
}

template <typename T>
T node<T>::getData()
{
    return this->_data;
}
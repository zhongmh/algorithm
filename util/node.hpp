#ifndef NODE_H_
#define NODE_H_

template <typename T>
class node
{
private:
    T _data;
    node<T> *_preNode;
    node<T> *_nextNode;

public:
    node(T data);
    node(T data, node<T> *pre, node<T> *next);
    void setPreNode(node<T> *preNode);
    void setNextNode(node<T> *nextNode);
    node<T> *getPreNode();
    node<T> *getNextNode();
    T getData();
};

#endif
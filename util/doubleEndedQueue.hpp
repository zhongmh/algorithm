#ifndef DUBLE_ENDED_QUEUE_H_
#define DUBLE_ENDED_QUEUE_H_
#include "node.hpp"
template <typename T>
class DoubleEndedQueue
{
private:
    node<T> *_firstNode;
    node<T> *_lastNode;

public:
    DoubleEndedQueue();
    void addFirst(T data);
    void addLast(T data);
    T pollFirst();
    T pollLast();
    T peekFirst();
    T peekLast();
    bool isEmpty();
};
#endif
#include <iostream>
#include "doubleEndedQueue.hpp"
#include "node.cpp"

template <typename T>
DoubleEndedQueue<T>::DoubleEndedQueue()
{
}

template <typename T>
void DoubleEndedQueue<T>::addFirst(T data)
{
    node<T> *dataNode = new node(data);
    if (_firstNode == nullptr)
    {
        _firstNode = dataNode;
    }
    else
    {
        _firstNode->setPreNode(dataNode);
        dataNode->setNextNode(_firstNode);
        _firstNode = dataNode;
    }
    if (_lastNode == nullptr)
    {
        _lastNode = dataNode;
    }
}

template <typename T>
void DoubleEndedQueue<T>::addLast(T data)
{
    node<T> *dataNode = new node(data);
    if (_lastNode == nullptr)
    {
        _lastNode = dataNode;
    }
    else
    {
        _lastNode->setNextNode(dataNode);
        dataNode->setPreNode(_lastNode);
        _lastNode = dataNode;
    }
    if (_firstNode == nullptr)
    {
        _firstNode = dataNode;
    }
}

template <typename T>
T DoubleEndedQueue<T>::pollFirst()
{
    if (isEmpty())
    {
        throw;
    }
    node<T> *node = _firstNode;
    if (_firstNode->getNextNode() != nullptr)
    {
        _firstNode = _firstNode->getNextNode();
    }
    else
    {
        _firstNode = nullptr;
        _lastNode = nullptr;
    }
    T data = node->getData();
    delete node;
    return data;
}
template <typename T>
T DoubleEndedQueue<T>::pollLast()
{
    if (isEmpty())
    {
        throw;
    }
    node<T> *node = _lastNode;
    if (_lastNode->getPreNode() != nullptr)
    {
        _lastNode = _lastNode->getPreNode();
    }
    else
    {
        _firstNode = nullptr;
        _lastNode = nullptr;
    }
    T data = node->getData();
    delete node;
    return data;
}

template <typename T>
bool DoubleEndedQueue<T>::isEmpty()
{
    return _lastNode == nullptr && _firstNode == nullptr;
}

template <typename T>
T DoubleEndedQueue<T>::peekFirst()
{
    if (isEmpty())
    {
        throw;
    }
    return _firstNode->getData();
}

template <typename T>
T DoubleEndedQueue<T>::peekLast()
{
    if (isEmpty())
    {
        throw;
    }
    return _lastNode->getData();
}

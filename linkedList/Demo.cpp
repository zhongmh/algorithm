#include "LinkedList.h"
#include <iostream>
#include <stdio.h>

using namespace std;

int main() {
    LinkedList* linkendList = CLInit();;
    char key[10];
    cout << "链表操作演示！" << endl;
    cout << "链表初始化完成" << endl;
    int o;
    DATA data;
    cout << "输入你的操作：" << endl;
    cout << "0 从头部插入节点" << endl;
    cout << "1 从尾部插入节点" << endl;
    cout << "2 从指定key之后插入节点" << endl;
    cout << "3 移除指定key的一个节点" << endl;
    cout << "4 移除指定key的节点" << endl;
    cout << "5 查询指定key的节点" << endl;
    cout << "6 打印链表节点" << endl;
    cout << "-1 退出" << endl;
    do {
        cout << "输入你的操作:";
        fflush(stdin);
        scanf("%d", &o);
        if (o == -1) {
            break;
        }
        if (o == 0) {
            fflush(stdin);
            cout << "输入你要插入的数据:";
            scanf("%s%s%d", &data.key, &data.name, &data.age);
            CLAddFirst(linkendList, data);
            cout << "添加成功" << endl;
        }
        if (o == 1) {
            fflush(stdin);
            cout << "输入你要插入的数据:";
            scanf("%s%s%d", &data.key, &data.name, &data.age);
            CLAddEnd(linkendList, data);
            cout << "添加成功" << endl;
        }
        if (o == 2) {

        }
        if (o == 3) {
            fflush(stdin);
            cout << "要移除顺序表的节点关键字:";
            scanf("%s", &key);
            CLDeleteNode(linkendList, key);

        }
        if (o == 4) {

        }
        if (o == 5) {
            fflush(stdin);
            cout << "要查找节点的关键字：";
            scanf("%s", &key);
            DATA pdata = CLFindNodeData(linkendList, key);
            if (pdata.key != NULL) {
                cout << "" << key << "节点为：";
                cout << "(" << pdata.key << ",";
                cout << pdata.name << ",";
                cout << pdata.age << ")" << endl;
            }
        }
        if (o == 6) {
            cout << "链表中的节点顺序为:" << endl;
            CLALLNode(linkendList);
        }
    } while (1);
    cout << "演示结束" << endl;
    return 0;
}
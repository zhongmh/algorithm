#include "LinkedList.h"
#include <string.h>
#include<stdio.h>
#include <iostream>

using namespace std;

Node* createNode(DATA nodeData);
Node* CLFindNode(LinkedList* CL, char* key);

LinkedList* CLInit() {
    LinkedList* linkedList;
    linkedList = (LinkedList*)malloc(sizeof(Node));
    if (linkedList == NULL) {
        cout << "内存申请失败" << endl;
    }
    else {
        linkedList->size = 0;
    }
    linkedList->firstNode = NULL;
    linkedList->endNode = NULL;
    return linkedList;
}

int CLAddEnd(LinkedList* CL, DATA nodeData) {
    Node* nodetemp = createNode(nodeData);
    if (nodetemp == NULL) {
        return -1;
    }
    if (CL->endNode == NULL) {
        CL->firstNode = nodetemp;
        CL->endNode = nodetemp;
    }
    else {
        CL->endNode->nextNode = nodetemp;
        nodetemp->preNode = CL->endNode;
        CL->endNode = nodetemp;
    }
    CL->size++;
    return 1;
}

int CLAddFirst(LinkedList* CL, DATA nodeData) {
    Node* nodetemp = createNode(nodeData);
    if (nodetemp == NULL) {
        return -1;
    }
    if (CL->firstNode == NULL) {
        CL->firstNode = nodetemp;
        CL->endNode = nodetemp;
    }
    else {
        nodetemp->nextNode = CL->firstNode;
        nodetemp->nextNode->preNode = nodetemp;
        CL->firstNode = nodetemp;
    }
    CL->size++;
    return 1;
}

DATA CLFindNodeData(LinkedList* CL, char* key) {
    Node* node = CLFindNode(CL, key);
    if (node == NULL) {
        DATA data;
        return data;
    }
    return node->nodeData;
}

int CLInsertNode(LinkedList* CL, char* findKey, DATA NodeData) {
    Node* node = CLFindNode(CL, findKey);
    if (node == NULL) {
        return -1;
    }
    Node* nodetemp = createNode(NodeData);
    if (nodetemp == NULL) {
        return -1;
    }
    if (node->nextNode == NULL) {
        node->nextNode = nodetemp;
        CL->endNode = nodetemp;
    }
    else {
        nodetemp->nextNode = node->nextNode;
        node->nextNode = nodetemp;
    }
    CL->size++;
}

int CLDeleteNode(LinkedList* CL, char* key) {
    Node* node = CLFindNode(CL, key);
    if (node == NULL) {
        cout << "not found key:" << key << endl;
        return -1;
    }
    if (node->preNode == NULL) {
        if (node->nextNode == NULL) {
            CL->firstNode = NULL;
            free(node);
            return 1;
        }
        CL->firstNode = node->nextNode;
        return 1;
    }
    if (node->nextNode == NULL) {
        node->preNode->nextNode = NULL;
        if (CL->firstNode == node) {
            CL->firstNode = NULL;
        }
        if (CL->endNode == NULL) {
            CL->endNode = NULL;
        }
        free(node);
        return 1;
    }
    node->preNode->nextNode = node->nextNode;
    node->nextNode->preNode = node->preNode;
     free(node);
    return 1;
}

int CLLength(LinkedList* CL) {
    return CL->size;
}

void CLALLNode(LinkedList* head) {
    Node* node = head->firstNode;
    while (node != NULL) {
        cout << "(" << node->nodeData.key << ",";
        cout << node->nodeData.name << ",";
        cout << node->nodeData.age << ")" << endl;
        if (node->nextNode == NULL) {
            return;
        }
        node = node->nextNode;
    }
}

Node* createNode(DATA nodeData) {
    Node* nodetemp;
    nodetemp = (Node*)malloc(sizeof(Node));
    if (nodetemp == NULL) {
        cout << "内存申请失败" << endl;
        return NULL;
    }
    nodetemp->nodeData = nodeData;
    nodetemp->preNode = NULL;
    nodetemp->nextNode = NULL;
    return nodetemp;
}

Node* CLFindNode(LinkedList* CL, char* key) {
    Node* node = CL->firstNode;
    while (node != NULL) {
        if (strcmp(node->nodeData.key, key) == 0) {
            return node;
        }
        node = node->nextNode;
    }
    return NULL;
}

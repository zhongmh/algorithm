typedef struct {
    char key[10];
    char name[10];
    int age;
} DATA;

typedef struct Node
{
    DATA nodeData;
    struct Node* preNode;
    struct Node* nextNode;
} Node;

typedef struct  
{
    Node *firstNode;
    Node *endNode;
    int size;
} LinkedList;



LinkedList *CLInit();

int CLAddEnd(LinkedList *CL,DATA nodeData);

int CLAddFirst(LinkedList *CL,DATA nodeData);

DATA CLFindNodeData(LinkedList *CL,char *key);

int CLInsertNode(LinkedList *CL,char *findKey,DATA NodeData);

int CLDeleteNode(LinkedList *CL,char *key);

int CLLength(LinkedList *CL);

void CLALLNode(LinkedList *head);
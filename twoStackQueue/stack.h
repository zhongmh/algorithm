typedef struct stack
{
    int array[10];
    int len;
    int pos;
} stack;

stack *NewStack();
int stackPush(stack *stack, int data);
int stackPop(stack *stack);
int stackEmpty(stack *stack);
int stackPeek(stack *stack);

int getAndRemovelastElement(stack *stack);
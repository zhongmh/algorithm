#include "stack.h"

typedef struct stackQueue
{
    stack *stackPush;
    stack *stackPop;
} stackQueue;

stackQueue *NewStackQueue();

int add(stackQueue *queue, int data);

int poll(stackQueue *queue);

int peek(stackQueue *queue);
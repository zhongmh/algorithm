#include "stackQueue.h"
#include <stdlib.h>
#include <stdio.h>

stackQueue *NewStackQueue()
{
    stackQueue *queue = malloc(sizeof(*queue));
    if (queue == NULL)
        return NULL;
    queue->stackPop = NewStack();
    queue->stackPush = NewStack();
    return queue;
}

void pushToPop(stackQueue *queue)
{
    if (stackEmpty(queue->stackPop))
        while (!stackEmpty(queue->stackPush))
            stackPush(queue->stackPop, stackPop(queue->stackPush));
}
int queueEmpty(stackQueue *queue)
{
    return stackEmpty(queue->stackPop) && stackEmpty(queue->stackPush);
}
int add(stackQueue *queue, int data)
{
    int answer = stackPush(queue->stackPush, data);
    pushToPop(queue);
    return answer;
}

int poll(stackQueue *queue)
{
    if (queueEmpty(queue))
    {
        printf("queue is empty\n");
        return -1;
    }
    pushToPop(queue);
    return stackPop(queue->stackPop);
}

int peek(stackQueue *queue)
{
    if (queueEmpty(queue))
    {
        printf("queue is empty\n");
        return -1;
    }
    pushToPop(queue);
    return stackPeek(queue->stackPop);
}

#include "stackQueue.h"
#include <stdio.h>
#include <stdlib.h>
void main()
{
    stackQueue *queue = NewStackQueue();
    for (int i = 0, data = rand() % 100; i < 15; i++, data = rand() % 100)
        printf("add(queue, %d)->%d\n", data, add(queue, data));
    for (int i = 0; i < 15; i++)
        printf("%d pop->%d\n", i, poll(queue));

    // test getAndRemovelastElement
    stack *stack = NewStack();
    for (int i = 0, data = rand() % 100; i < 10; i++, data = rand() % 100)
        printf("stackPush(stack, %d)->%d\n", data, stackPush(stack, data));
    for (int i = 0; i < 10; i++)
        printf("%d getAndRemovelastElement->%d\n", i, getAndRemovelastElement(stack));
}
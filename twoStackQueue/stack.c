#include "stack.h"
#include <stdlib.h>
#include <stdio.h>

stack *NewStack()
{
    stack *stack = malloc(sizeof(*stack));
    if (stack == NULL)
        return NULL;
    stack->pos = -1;
    stack->len = 10;
    return stack;
}

int stackPush(stack *stack, int data)
{
    if (stack->pos + 1 >= stack->len)
    {
        printf("array already full,you can't push data\n");
        return -1;
    }
    stack->array[++stack->pos] = data;
    return 1;
}
int stackPop(stack *stack)
{
    if (stackEmpty(stack))
    {
        return -1;
    }
    return stack->array[stack->pos--];
}
int stackEmpty(stack *stack)
{
    return stack->pos == -1;
}

int stackPeek(stack *stack)
{
    if (stackEmpty(stack))
    {
        printf("stack is empty\n");
        return -1;
    }
    return stack->array[stack->pos];
}

int getAndRemovelastElement(stack *stack)
{
    int result = stackPop(stack);
    if (stackEmpty(stack))
        return result;
    else
    {
        int last = getAndRemovelastElement(stack);
        stackPush(stack, result);
        return last;
    }
}
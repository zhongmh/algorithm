#include <iostream>

#define  SWAP(a, b){ int tmp; tmp = a;a = b;b = tmp;}
using namespace std;

void quick_sort_sub(int *data, int left, int right) {
    int left_index = left;
    int right_index = right;
    int pivot = data[(right + left) / 2];
    while (left_index <= right_index) {
        for (; data[left_index] < pivot; left_index++);
        for (; data[right_index] > pivot; right_index--);
        if (left_index <= right_index) {
            SWAP(data[left_index], data[right_index]);
            left_index++;
            right_index--;
        }
    }
    if (right_index > left) {
        quick_sort_sub(data, left, right_index);
    }
    if (left_index < right) {
        quick_sort_sub(data, left_index, right);
    }
}

void quick_sort(int data[], int data_size) {
    quick_sort_sub(data, 0, data_size - 1);
}

int find(int e, int S[], int start, int end) {
    if ((start == end && e != S[end]) || start + 1 == end) {
        return -1;
    }
    if (S[start] == e) {
        return start;
    }
    if (S[end] == e) {
        return end;
    }
    int i = (start + end) / 2;
    if (S[i] > e) {
        return find(e, S, start, i);
    }
    if (S[i] < e) {
        return find(e, S, i, end);
    }
    return i;
}

int main() {
    int n, q;
    int S[200000];
    cin >> n;
    int i, t, an = 0;
    for (i = 0, t = 0; i < n; i++) {
        cin >> S[i];
    }
    quick_sort(S, i);
    cin >> q;
    for (n = i, i = 0; i < q; i++) {
        cin >> t;
        if (find(t, S, 0, n) != -1) {
            an++;
        }
    }
    cout << an << endl;
}
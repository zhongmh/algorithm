#include <iostream>

using namespace std;

int main() {
    int n, i, j, tmp, lastExchangeIndex, count;
    bool isSwap = false;
    cin >> n;
    int *data = (int *) malloc(sizeof(int) * n);
    for (i = 0; i < n; i++) {
        cin >> data[i];
    }
    for (i  = count = 0; i < n - 1; i++) {
        isSwap = false;
        for (j = 0; j < n - i - 1; j++) {
            if (data[j] > data[j + 1]) {
                tmp = data[j];
                data[j] = data[j + 1];
                data[j + 1] = tmp;
                isSwap = true;
                count++;
                lastExchangeIndex = j;
            }
        }
        if (!isSwap) {
            break;
        }
        i = n - lastExchangeIndex - 2;
    }
    printf("%d\n", count);
    free(data);
    return 1;
}
#include <iostream>
#include <set>
#include <cstring>

using namespace std;

int main()
{
    set<string> container;
    int n;
    cin >> n;
    string cmd, key;
    for (; n > 0; n--)
    {
        cin >> cmd;
        cin >> key;
        if(cmd == "insert"){
            container.insert(key);
        }
        if(cmd == "find"){
            if(container.count(key) != 0){
                cout << "yes" << endl;
            }else{
                cout << "no" << endl;
            }
        }
    }
}
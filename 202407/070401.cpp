#include <iostream>
#include <cstring>
using namespace std;
int find(int e,int S[],int start,int end){
	if((start == end && e != S[end]) || start + 1 == end){
            return -1;
        }
        if(S[start] == e){
            return start;
        }
        if(S[end] == e){
            return end;
        }
        int i = (start+end) / 2;
        if(S[i] > e){
            return find(e,S,start,i);
        }
        if(S[i] < e){
            return find(e,S,i,end);
        }
        return i;
}
void insert(int e, int S[], int pi, int len) {
        for (; pi < len; len--) {
            S[len] = S[len - 1];
        }
        S[pi] = e;
}
    
// 插入元素到位置i,i后面的元素向后移动
void insertSort(int e, int S[], int len) {
        int li = 0, ri = len, pi;
        if (e <= S[li] || li == ri) {
            insert(e, S, li, len);
            return;
        }
        ri--;
        if (e >= S[ri]) {
            insert(e, S, len, len);
            return;
        }
        while (true) {
            if (li + 1 == ri) {
                if (S[ri] > e) {
                    insert(e, S, ri, len);
                    break;
                }
                if (S[li] > e) {
                    insert(e, S, li, len);
                    break;
                }
            }
            pi = (li + ri) / 2;
            if (S[pi] == e) {
                // 相等，直接在这里插入
                insert(e, S, pi, len);
                break;
            }
            if (S[pi] < e) {
                li = pi;
            } else if (S[pi] > e) {
                ri = pi;
            }
        }
    }

int main(){
	int n,q;
    int S[10000];
	cin >> n;
	int i,t,an;
	for(i=0,t = 0;i<n;i++){
		cin >> t;
		insertSort(t,S,i);
	}
	cin >> q;
	for(n = i,i=0;i<q;i++){
		cin >> t;
		if(find(t,S,0,n) != -1){
			an++;
		}
	}
	cout << an << endl;
}
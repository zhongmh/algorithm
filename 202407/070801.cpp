#include <iostream>
#include <array>
using namespace std;

int main(){
    int n,k,w,total_w = 0,i,P,j,c,max = 0;
    cin >> n,cin >> k;
    int* packages = new int[n]; 
    for(i=0;i<n;i++){
        cin >> w;
        packages[i] = w;
        total_w += w;
        max = max < w ? w : max;
    }
    int average_load = (total_w / k) + (total_w % k == 0 ? 0 :1);
    P = average_load > max ? average_load : max;
    for(i=0, j=0,c = 0;i<n;){
        w = packages[i];
        j+=w;
        if(j == P){
            c++;
            j=0;
        }else if(j > P){
            c++;
            j = w;
        }
        if(++i == n){
            if((j == 0 && c <= k) || ++c <= k){
                break;
            }else{
                P++; // 增加载重，从新计算
                c = i = j = 0;
            }
        }
    }
    delete[] packages;
    cout << P << endl;
}
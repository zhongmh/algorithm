#include "ThreadedBinaryTree.hpp"
#include <iostream>

void InOrder(ThreadedBinaryTreeNode *node)
{
    if (node != nullptr)
    {
        if(node->lTag == 0)
              InOrder(node->left);
        std::cout << "Node(" << node->data << ")" << std::endl;
        if(node->rTag == 0)
            InOrder(node->right);
    }
}

int main() {
    ThreadedBinaryTree tree(1, "56");
    tree.insertNode("50");
    tree.insertNode("80");
    tree.insertNode("40");
    tree.insertNode("48");
    tree.insertNode("36");
    tree.insertNode("90");
    tree.insertNode("72");
    tree.insertNode("99");
    InOrder(tree.getRootNode());
    tree.inorderTaversal();
}


#include "BinaryTree.hpp"
#include <iostream>

int main()
{
    BinaryTreeNode *root = insertNode(nullptr, "50");
    insertNode(root, "46");
    insertNode(root, "89");
    insertNode(root, "90");
    insertNode(root, "72");
    int size = binaryTreeSize(root);
    std::cout << "tree size is " << size << std::endl;
    PreOrder(root);
    PreOrderNonRecursive(root);
    std::cout << "tree height is " << binaryTreeHeight(root) << std::endl;
    PostOrder(root);
    LevelOrder(root);
}
typedef struct BalanceTreeNode
{
    const char *data;
    /** 左节点 */
    struct TreeNode *left;
    /** 右节点 */
    struct TreeNode *right;
    /** 父节点*/
    struct TreeNode *parent;
    /** 根节点 */
    struct TreeNode *root;

} BalanceTreeNode;
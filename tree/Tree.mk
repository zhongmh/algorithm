tree-build:
	g++ -g tree/AVLTree.cpp tree/AVLTreeDemo.cpp -o tree/AVLTreeDemo.out
	g++ -g tree/BinarySearchTree.cpp tree/BinarySearchTreeDemo.cpp -o tree/BinarySearchTreeDemo.out
	g++ -g tree/ThreadedBinaryTree.cpp tree/ThreadedBinaryTreeDemo.cpp -o tree/ThreadedBinaryTreeDemo.out
clean:
	rm -f */*.out
	rm -f */*.exe
	rm -f */*.o
#include "BinarySearchTree.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int main()
{
    BinarySearchTree tree("56");
    if (tree.isBST())
        printf("The tree is blanace search tree\n");
    else
        printf("The tree isn't blanace search tree\n");
    tree.insertNode("40");
    tree.insertNode("88");
    tree.insertNode("23");
    tree.insertNode("36");
    tree.insertNode("46");
    tree.insertNode("67");
    tree.insertNode("99");
    if (tree.isBST())
        printf("The tree is blanace search tree\n");
    else
        printf("The tree isn't blanace search tree\n");
    BinarySearchNode *maxNode = tree.findMax();
    if (maxNode != nullptr)
        printf("found max node(%s)\n", maxNode->data);
    else
        printf("not found max node\n");
    BinarySearchNode *minNode = tree.findMin();
    if (maxNode != nullptr)
        printf("found min node(%s)\n", minNode->data);
    else
        printf("not found min node\n");

    const char *data = "99";
    BinarySearchNode *node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    data = "99";
    tree.remove(data);
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    data = "36";
    tree.remove(data);
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    data = "23";
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    if (tree.isBST())
        printf("The tree is blanace search tree\n");
    else
        printf("The tree isn't blanace search tree\n");
    data = "67";
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    printf("execute tree.pruneBST(\"40\", \"80\")\n");
    tree.pruneBST("40", "80");

    data = "40";
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    data = "23";
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    data = "99";
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    data = "88";
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    data = "67";
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
    data = "46";
    node = tree.find(data);
    if (node != nullptr)
        printf("found node(%s)\n", node->data);
    else
        printf("not found node(%s)\n", data);
}
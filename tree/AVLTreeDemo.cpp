#include "AVLTree.hpp"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
    const char *d1 = "56";
    const char *d2 = "36";
    printf("strcmp(%s,%s) = %d\n", d1, d2, strcmp(d1, d2));
    printf("strcmp(%s,%s) = %d\n", d2, d1, strcmp(d2, d1));

    AVLTree avlTree("56");
    avlTree.insert("72");
    avlTree.insert("36");
    avlTree.insert("81");
    avlTree.insert("35");
    avlTree.insert("64");
    avlTree.insert("34");
    avlTree.insert("48");
    avlTree.insert("33");
    if (avlTree.isAVL())
        printf("The Tree is AVLTree\n");
    else
        printf("The Tree isn't AVLTree\n");
}
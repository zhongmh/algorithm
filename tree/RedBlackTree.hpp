typedef struct TreeNode
{
    /** 数据*/
    const char *data;
    /** 当前节点相对于父节点所处的问题*/
    int pos;
    /** 颜色 1 红色 0 黑色*/
    bool red;
    /** 左节点 */
    struct TreeNode *left;
    /** 右节点 */
    struct TreeNode *right;
    /** 父节点*/
    struct TreeNode *parent;
    /** 根节点 */
    struct TreeNode *root;
} TreeNode;

/** 创建一个节点*/
TreeNode *buildNode(TreeNode *root, const char *data);

/** 左旋转*/
TreeNode *rotateLeft(TreeNode *root, TreeNode *p);

/** 右旋转*/
TreeNode *rotateRight(TreeNode *root, TreeNode *p);

/** 平衡删除*/
TreeNode *balanceDeletion(TreeNode *root, TreeNode *x);

/** 平衡插入*/
int balanceInsertion(TreeNode *root, TreeNode *x);
typedef struct BinaryTreeNode
{
    const char *data;
    BinaryTreeNode *right;
    BinaryTreeNode *left;
    BinaryTreeNode *root;
    BinaryTreeNode *parent;
} BinaryTreeNode;

int binaryTreeSize(BinaryTreeNode *root);

int binaryTreeHeight(BinaryTreeNode *root);

int level(BinaryTreeNode *root);

BinaryTreeNode *insertNode(BinaryTreeNode *root, const char *data);

BinaryTreeNode *getNode(BinaryTreeNode *root, const char *data);

int deleteNode(BinaryTreeNode *root, const char *data);

BinaryTreeNode *getLeastCommonAncestor(BinaryTreeNode nodes[]);

void PreOrder(BinaryTreeNode *root);

void InOrder(BinaryTreeNode *root);

void PostOrder(BinaryTreeNode *root);

void PreOrderNonRecursive(BinaryTreeNode *root);

void LevelOrder(BinaryTreeNode *root);

int IsIsomorphic(BinaryTreeNode *root1, BinaryTreeNode *root2);
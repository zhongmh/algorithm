#include <iostream>
#include <algorithm>
using namespace std;
const int N = 20;
int n,m;
int cat[N],sum[N]; // cat 表示猫的质量 sum 表示当前车的质量
int ans = N;
// u 表示当前第几只猫
// k 表示车的数量
void dfs(int u,int k){
	if(k >= ans)return;
	if(u == n){
		ans = k;
		return;
	}
	for( int i=0;i<k;i++){
		if(cat[u]+sum[i] <= m){
			sum[i] += cat[i];
			dfs(u+1,k);
			sum[i] -= cat[u]; //恢复现场
		}
	}
	sum[k] = cat[u];
	dfs(u+1,k+1);
	sum[k] = 0;
}

int main(){
	cin >> n >> m;
	for(int i=0;i<n;i++) cin>> cat[i];
	sort(cat,cat+n);// 从小到大排序
	reverse(cat,cat+n); // 翻转 从大到小排序
	dfs(0,0);
	cout << ans << endl;
	return 0;
}
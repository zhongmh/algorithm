#include <fstream>
#include <iostream>
using namespace std;
void Move(int t,char x,char y){
	cout << "move " << t << " from " << x << " to "<< y << endl;
}
void Hannoi(int n,char a,char b,char c){
	if(n == 1){
		Move(1,a,c);
	}else {
		Hannoi(n-1,a,c,b);
		Move(n,a,c);
		Hannoi(n-1,b,a,c);
	}
}

int main(){
	int n;
	scanf("%d",&n);
	Hannoi(n,'a','b','c');
	return 0;
}

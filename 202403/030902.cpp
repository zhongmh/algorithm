#include <iostream>
#include <list>
#include <stack>

#define BACK_SLASH '\\'
#define SLASH '/'
#define UNDERLINE '_'
#define END '\0'

using namespace std;

int main(){                                                                                                                                                                                                              
    char areas[20000];
	cin.getline(areas,20000);
	list<int> areaList;
	int i,j,deep,totalArea;
	float area = 0.0;
	for(i=0,totalArea = 0;areas[i] != END;){
		 if(areas[i] != BACK_SLASH){
			i++;
			continue;
		 }
		 for(j=i+1,deep = 1,area = 0.5;areas[j] != END;j++){
			if(areas[j] == BACK_SLASH){
				area += deep + 0.5;
				deep++;
			}else
			if(areas[j] == UNDERLINE){
				area += deep;
			}else
			if(areas[j] == SLASH){
				deep--;
				area += deep + 0.5;
			}
			if(deep == 0){
				i = j + 1;
				areaList.push_back(area);
				totalArea += areaList.back();
				break;
			}
		 }
		 if(areas[j] == END){
			i++;
		 }
	}
	cout << totalArea << endl;
	cout << areaList.size();
	while(!areaList.empty()){
		cout << " " << areaList.front();
		areaList.pop_front();
	}
	cout << endl;                                                                                                                                                  
}
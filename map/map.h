typedef struct TreeNode {
	void *data;
	int color;
	struct TreeNode *left;
	struct TreeNode *right;
} TreeNode;

int hash(const char *key);
TreeNode* createTree();
void* get(const char *key);
void put(TreeNode *root,const char *key,void *value);


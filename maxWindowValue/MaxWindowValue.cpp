#include "../util/doubleEndedQueue.cpp"
#include <iostream>
#include <stdlib.h>
#include <assert.h>

int main()
{
    DoubleEndedQueue<int> dqueue;
    assert(dqueue.isEmpty());
    dqueue.addFirst(2);
    assert(dqueue.peekFirst() == 2);
    assert(dqueue.peekLast() == 2);
    dqueue.addFirst(3);
    assert(dqueue.peekFirst() == 3);
    assert(dqueue.peekLast() == 2);
    assert(!dqueue.isEmpty());
    dqueue.addFirst(4);
    assert(dqueue.peekFirst() == 4);
    assert(dqueue.peekLast() == 2);

    assert(dqueue.pollFirst() == 4);
    assert(dqueue.peekLast() == 2);
    assert(dqueue.peekFirst() == 3);

    assert(dqueue.pollFirst() == 3);
    assert(dqueue.peekLast() == 2);
    assert(dqueue.peekFirst() == 2);
    assert(dqueue.pollFirst() == 2);
    assert(dqueue.isEmpty());

    int array[10];
    srand(time(NULL));
    for (int i = 0; i < 10; i++)
    {
        array[i] = rand() % 10;
        std::cout << array[i];
        if (i < 9)
        {
            std::cout << ",";
        }
        else
        {
            std::cout << std::endl;
        }
    }
    int i = 0;
    int windowWidth = 3;
    int res[7];
    int index = 0;
    for (; i < 10; i++)
    {
        while (!dqueue.isEmpty() && array[dqueue.peekFirst()] <= array[i])
        {
            dqueue.pollLast();
        }
        dqueue.addFirst(i);
        if (dqueue.peekLast() == i - windowWidth)
        {
            dqueue.pollLast();
        }
        if (i >= windowWidth - 1)
        {
            res[index++] = array[dqueue.peekLast()];
        }
    }
    std::cout << "MaxWindowValueArray: ";
    for (i = 0; i < 7; i++)
    {
        std::cout << res[i];
        if (i < 6)
        {
            std::cout << ",";
        }
        else
        {
            std::cout << std::endl;
        }
    }
}
#include "SequentialList.h"
#include <iostream>
#include <stdio.h>

using namespace std;

int main(){
    SLType SL;
    DATA data;
    DATA *pdata;
    char key[10];
    cout << "顺序表操作演示！" << endl;
    SLInit(&SL);
    cout << "顺序表初始化完成" << endl;

    do {
        cout  << "输入添加的节点" << endl;
        fflush(stdin);
        scanf("%s%s%d",&data.key,&data.name,&data.age);
        if(data.age){
            if(!SLAdd(&SL,data)){
                break;
            }
        }else {
            break;
        }
    }while (1);
    
    cout << "顺序表中的节点顺序为:" << endl;
    SLAll(&SL);
    fflush(stdin);
    cout << "要取出的顺序节点的序号：" << endl;
    int i;
    scanf("%d",&i);
    pdata = SLFindByNum(&SL,i);
    if(pdata) {
        cout << "第" << i << "个节点为：";
        cout << "(" << pdata->key << ",";
        cout << pdata->name << ",";
        cout << pdata->age  << ")" << endl;
    }
    cout << "要查找节点的关键字：";
    scanf("%s",&key);
    i = SLFindByCount(&SL,key);
    pdata = SLFindByNum(&SL,i);
    if(pdata) {
        cout << "第" << i << "个节点为：";
        cout << "(" << pdata->key << ",";
        cout << pdata->name << ",";
        cout << pdata->age  << ")" << endl;
    }
    cout << "要移除顺序表的节点顺序:";
    scanf("%d",&i);
    SLDelete(&SL,i);
    cout << "\n顺序表中的数据：" << endl;
    SLAll(&SL);
    return 0;
}
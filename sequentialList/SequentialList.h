#define MAXLEN 100

typedef struct 
{
    char key[10];
    char name[20];
    int age;
} DATA;

typedef struct {
    DATA ListData[MAXLEN];
    int ListLen;
} SLType;

/** 初始化顺序表 */
void SLInit(SLType *SL);

/** 获取顺序表的长度 */
int SLLength(SLType *SL);

/* 插入节点 */
int SLInsert(SLType *SL,int n,DATA data);

/* 增加节点 */
int SLAdd(SLType *SL,DATA data);

/* 删除节点 */
int SLDelete(SLType *SL,int n);

/* 返回顺序表中的数据 */
DATA *SLFindByNum(SLType *SL,int n);

/* 根据关键字查询节点 */
int SLFindByCount(SLType *SL,char *key);

/* 打印顺序表中的数据 */
int SLAll(SLType *SL);
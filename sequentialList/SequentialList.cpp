#include "SequentialList.h"
#include <iostream>
#include <string.h>
using namespace std;

void SLInit(SLType *SL){
    SL->ListLen = 0;
}

int SLLength(SLType *SL){
    return SL->ListLen;
}

int SLInsert(SLType *SL,int n,DATA data){
    if(SL->ListLen == MAXLEN){
        cout << "顺序表已满，不能插入节点。" << endl;
        return -1;
    }
    if( n < 0 || n > 99){
        cout << "Index of range:" << n << endl;
        return -1;
    }
    SL->ListLen++;
    for(int i=SL->ListLen -1;i> n;i--){
        SL->ListData[i] = SL->ListData[i-1]; 
    }
    SL->ListData[n-1] = data;
    return 1;
}

int SLAdd(SLType *SL,DATA data){
    if(SL->ListLen == MAXLEN){
        cout << "顺序表已满，不能插入节点。" << endl;
        return -1;
    }
    SL->ListData[SL->ListLen++] = data;
    return 1;
}

int SLDelete(SLType *SL,int n){
    if( n < 0 || n > 99){
        cout << "Index of range:" << n << endl;
        return -1;
    }
    SL->ListLen--;
    for(int i=n-1;i<SL->ListLen;i++){
        SL->ListData[i] = SL->ListData[i+1];
    }
    SL->ListData[SL->ListLen+1];
    return 1;
}


DATA *SLFindByNum(SLType *SL,int n){
    if( n < 0 || n > 99){
        cout << "Index of range:" << n << endl;
        return NULL;
    }
    return &SL->ListData[n]; 
}

int SLFindByCount(SLType *SL,char *key){
    for(int i=0;i<SL->ListLen;i++){
        if(strcmp(SL->ListData[i].key,key) == 0){
            return i;
        }
    }
    return -1;
}

int SLAll(SLType *SL){
    for (int i=0;i<SL->ListLen;i++){
        cout << "(" << SL->ListData[i].key << ",";
        cout << SL->ListData[i].name << ",";
        cout << SL->ListData[i].age  << ")" << endl;
    }
    return 1;
}
#include "Permutations.hpp"
#include<cstdio>
// 行列式中的全排列求解
int main(){
    Permutations p(10);
    p.printArray();
    printf("逆序数：%d\n",p.inversionNumber());
    if(p.oddPermutation())
        printf("偶排列\n");
    else 
        printf("奇排列\n");
    int i =2,j=4;
    printf("exchange(%d,%d)\n",i,j);
    p.exchange(i,j);
    p.printArray();
    printf("交换后逆序数：%d\n",p.inversionNumber());
    if(p.oddPermutation())
        printf("偶排列\n");
    else 
        printf("奇排列\n");

    i =5,j=8;
    printf("exchange(%d,%d)\n",i,j);
    p.exchange(i,j);
    p.printArray();
    printf("交换后逆序数：%d\n",p.inversionNumber());
    if(p.oddPermutation())
        printf("偶排列\n");
    else 
        printf("奇排列\n");
    
    i =1,j=9;
    printf("exchange(%d,%d)\n",i,j);
    p.exchange(i,j);
    p.printArray();
    printf("交换后逆序数：%d\n",p.inversionNumber());
    if(p.oddPermutation())
        printf("偶排列\n");
    else 
        printf("奇排列\n");
}
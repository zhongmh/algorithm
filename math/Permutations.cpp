#include "Permutations.hpp"
#include<cstdio>
#include<cstdlib>

Permutations::Permutations(int capacity){
    this->capacity = capacity > MAX_LEN ? MAX_LEN : capacity;
    for(int i=0;i<capacity;i++)
    this->array[i]=rand() %(100)+1;
}

int Permutations::isInversionNumber(int s,int i){
    return this->array[s] < this->array[i];
}


int Permutations::inversionNumber(int s,int i){
    if(s == capacity){
        return 0;
    }
    if(i == capacity){
        return inversionNumber(s+1,s+2);
    }
    return (isInversionNumber(s,i) ? 1 : 0) + inversionNumber(s,i+1); 
}


int Permutations::inversionNumber(){
    return inversionNumber(0,1);
}

void Permutations::printArray(){
    
    printf("[");
    for(int i=0;i<this->capacity;){
        printf("%d",this->array[i]);
        if(++i != capacity){
            printf(",");
        }else{
            printf("]\n");
        }
    }
}

int Permutations::oddPermutation(){
    return inversionNumber()%2==0;
}

void Permutations::exchange(int i,int j){
    int tmp = this->array[i];
    this->array[i] = this->array[j]; 
    this->array[j] = tmp;
}
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

double rand01(double *r) {
    double base, u, v, p, temp1, temp2, temp3;
    base = 256.0;
    u = 17.0;
    v = 139.0;
    temp1 = u * (*r) + v;
    temp2 = (int)(temp1 / base);
    temp3 = temp1 - temp2 * base;
    *r = temp3;
    p = *r / base;
    return p;
}

int main() {
    
    int i, j;
    srand((int)time(0));
    for (j = 0;j < 10;j++) {
        for (i = 0;i < 10;i++) {
            printf("%d\t", rand());
        }
        printf("\n");
    }
    double r;
    r = 5.0;
    printf("产生10个[0-1]随机数:\n");
    for (int i = 0;i < 10;i++) {
        printf("%10.5lf\n", rand01(&r));
    }
    r = 5.0;
    printf("产生10个[0-1]随机数:\n");
    for (int i = 0;i < 10;i++) {
        printf("%10.5lf\n", rand01(&r));
    }
    return 0;
}


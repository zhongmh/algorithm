#include <stdio.h>

int LeapYear(int year);

int main() {
    for (int year = 2000;year < 3000;year++) {
        if (LeapYear(year)) {
            printf("%d,", year);
        }
    }
    return 0;
}

int LeapYear(int year) {
    return (year % 400 == 0 || (year % 100 != 0)) && (year % 4 == 0);
}


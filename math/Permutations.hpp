#define MAX_LEN 100
class Permutations {

private:
    int array[MAX_LEN];
    int capacity;
    int isInversionNumber(int s,int i);
    int inversionNumber(int s,int i);
public:
    Permutations(int capacity);

    /**
     * 求出全排列中的逆序数
    */
    int inversionNumber();

    void printArray();

    int oddPermutation();

    void exchange(int i,int j);
};
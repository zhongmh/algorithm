#include "radix.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char *argv[])
{

    char *data = "hel";
    raxSetDebugMsg(1);
    rax *rax = raxNew();
    raxInsert(rax, data, strlen(data) + 1, data, NULL);
    data = "hello";
    raxInsert(rax, data, strlen(data) + 1, data, NULL);
    data = "world";
    raxInsert(rax, data, strlen(data) + 1, data, NULL);
    data = "happy";
    raxInsert(rax, data, strlen(data) + 1, data, NULL);
    data = "pupil";
    raxInsert(rax, data, strlen(data) + 1, data, NULL);
    data = "tree";
    raxInsert(rax, data, strlen(data) + 1, data, NULL);
    data = "radix";
    raxInsert(rax, data, strlen(data) + 1, data, NULL);
    if (errno != -1)
    {
        printf("error:%s\n", strerror(errno));
    }
    return 1;
}
